﻿# MisterGames Bezier v0.1.2

## Usage
- Use SplineCreator to create spline: [tutorial](https://www.youtube.com/watch?v=saAQNRSYU9k&t=2s)
- Use SplineTool to create spline from parameters
- Use SplinePrefabPlacer to place prefabs along spline
- Use SplineMeshGenerator to generate mesh along spline 

## Assembly definitions
- MisterGames.Bezier
- MisterGames.Bezier.Editor

## Installation
- Add [MisterGames Common](https://gitlab.com/theverymistergames/common/) package
- Top menu MisterGames -> Packages, add packages: 
  - [Bezier](https://gitlab.com/theverymistergames/bezier/) 
